﻿using Unity;
using Unity.Lifetime;

namespace NorimValgyt.App_Start
{
    public static class Injector
    {
        private static readonly UnityContainer _container = new UnityContainer();

        public static void Register<I, T>() where T : I
        {
            _container.RegisterType<I, T>(new PerResolveLifetimeManager());
        }

        public static void Register<T>()
        {
            _container.RegisterType<T>(new PerResolveLifetimeManager());
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }
    }
}