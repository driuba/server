﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using NorimValgyt.Context;
using NorimValgyt.Dto;
using NorimValgyt.Models;
using NorimValgyt.Services;
using System.Collections;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using Unity;
using Unity.WebApi;

namespace NorimValgyt.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.DependencyResolver = new UnityDependencyResolver(new UnityContainer());
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(corsAttr);
            InitDependencies();

            InitOData(config);
        }

        private static void InitDependencies()
        {
            Injector.Register<NvDbContext>();

            Injector.Register<DishesService>();
            Injector.Register<GoogleApi>();
        }

        private static void InitOData(HttpConfiguration config)
        {
            var builder = new ODataConventionModelBuilder();

            config.Count().Select().Filter().Expand().OrderBy().MaxTop(null);

            builder.EntitySet<Administrator>("Administrators");
            builder.EntitySet<Answer>("Answers");
            builder.EntitySet<Carrier>("Carriers");
            builder.EntitySet<DeliveryInvoice>("DeliveryInvoices");
            builder.EntitySet<Dish>("Dishes");
            builder.EntitySet<Feedback>("Feedbacks");
            builder.EntitySet<Location>("Locations");
            builder.EntitySet<PurchaseInvoice>("PurchaseInvoices");
            builder.EntitySet<Question>("Questions");
            builder.EntitySet<Restaurant>("Restaurants");
            builder.EntitySet<Survey>("Surveys");
            builder.EntitySet<Tag>("Tags");
            builder.EntitySet<User>("Users");

            builder.Namespace = "CustomMethods";

            var action = builder.EntityType<Dish>().Action("Order");

            action.Parameter<DishOrderRequest>("request");
            action.Returns<DishOrderResponse>();

            action = builder.EntityType<Answer>().Collection.Action("Many");

            action.CollectionParameter<Answer>("answers");
            action.ReturnsCollectionFromEntitySet<Answer>("Asnwers");

            action = builder.EntityType<Answer>().Collection.Action("Suggestions");

            action.Parameter<SuggestionRequest>("request");
            action.ReturnsCollectionFromEntitySet<Restaurant>("Restaurants");

            action = builder.EntityType<Dish>().Collection.Action("Suggestions");

            action.Parameter<SuggestionRequest>("request");
            action.ReturnsCollectionFromEntitySet<Restaurant>("Restaurants");

            action = builder.EntityType<Dish>().Action("AddTags");

            action.Parameter<DishTagsRequest>("request");

            action = builder.EntityType<Dish>().Action("RemoveTags");

            action.Parameter<DishTagsRequest>("request");

            config.MapODataServiceRoute(
                    routeName: "ODataRoute",
                    routePrefix: null,
                    model: builder.GetEdmModel());
        }
    }
}
