﻿using MySql.Data.Entity;
using NorimValgyt.Models;
using System.Configuration;
using System.Data.Entity;

namespace NorimValgyt.Context
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class NvDbContext : DbContext
    {
        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Carrier> Carriers { get; set; }
        public DbSet<DeliveryInvoice> DeliveryInvoices { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<PurchaseInvoice> PurchaseInvoices { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<User> Users { get; set; }

        public NvDbContext() : base(ConfigurationManager.ConnectionStrings["norim-valgyt"].ConnectionString) { }
    }
}