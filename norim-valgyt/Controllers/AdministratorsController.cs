﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class AdministratorsController : CRUDControllerBase<Administrator>
    {
        public AdministratorsController(NvDbContext context) : base(context) { }
    }
}