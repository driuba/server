﻿using Microsoft.AspNet.OData;
using NorimValgyt.Context;
using NorimValgyt.Dto;
using NorimValgyt.Models;
using NorimValgyt.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace NorimValgyt.Controllers
{
    public class AnswersController : CRUDControllerBase<Answer>
    {
        private readonly DishesService _dishesService;
        public AnswersController(NvDbContext context, DishesService dishesService) : base(context)
        {
            _dishesService = dishesService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Many(ODataActionParameters parameters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var answers = parameters["answers"] as IEnumerable<Answer>;

            _set.AddRange(answers);

            await _context.SaveChangesAsync();

            return Ok(answers);
        }

        [HttpPost]
        [EnableQuery]
        public IHttpActionResult Suggestions(ODataActionParameters parameters)
        {
            if (parameters is object && parameters.ContainsKey("request") && parameters["request"] is SuggestionRequest request)
            {
                var response = _dishesService.GetSuggestions(request);

                if (response.Success)
                {
                    return Ok(response.Restaurants);
                }

                return BadRequest(response.Message);
            }

            return BadRequest();
        }
    }
}