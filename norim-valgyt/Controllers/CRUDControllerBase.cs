﻿using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using NorimValgyt.Context;
using NorimValgyt.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace NorimValgyt.Controllers
{
    public abstract class CRUDControllerBase<TEntity> : ODataController where TEntity : EntityBase
    {
        protected readonly NvDbContext _context;
        protected readonly DbSet<TEntity> _set;

        public CRUDControllerBase(NvDbContext context)
        {
            _context = context;
            _set = context.Set<TEntity>();
        }

        [EnableQuery]
        public virtual IQueryable<TEntity> Get()
        {
            return _set.AsNoTracking();
        }

        [EnableQuery]
        public virtual SingleResult<TEntity> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_set.AsNoTracking().Where(s => s.Id == key));
        }

        public async Task<IHttpActionResult> Post(TEntity entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _set.Add(entity);

            await _context.SaveChangesAsync();

            return Created(entity);
        }

        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<TEntity> delta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entity = await _set.FindAsync(key);

            if (entity is null)
            {
                return NotFound();
            }

            delta.Patch(entity);

            await _context.SaveChangesAsync();

            return Updated(entity);
        }

        public async Task<IHttpActionResult> Put([FromODataUri] int key, TEntity entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != entity.Id)
            {
                return BadRequest();
            }

            _context.Entry(entity).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return Updated(entity);
        }

        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            var entity = await _set.FindAsync(key);

            if (entity is null)
            {
                return NotFound();
            }

            _set.Remove(entity);

            await _context.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}