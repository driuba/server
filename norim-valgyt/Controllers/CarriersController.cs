﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class CarriersController :CRUDControllerBase<Carrier>
    {
        public CarriersController(NvDbContext context) : base(context) { }
    }
}