﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class DeliveryInvoicesController : CRUDControllerBase<DeliveryInvoice>
    {
        public DeliveryInvoicesController(NvDbContext context) : base(context) { }
    }
}