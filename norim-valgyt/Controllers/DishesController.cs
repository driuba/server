﻿using Microsoft.AspNet.OData;
using NorimValgyt.Context;
using NorimValgyt.Dto;
using NorimValgyt.Models;
using NorimValgyt.Services;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace NorimValgyt.Controllers
{
    public class DishesController : CRUDControllerBase<Dish>
    {
        private readonly DishesService _dishesService;

        public DishesController(NvDbContext context, DishesService dishesService) : base(context)
        {
            _dishesService = dishesService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Order([FromODataUri] int key, ODataActionParameters parameters)
        {
            if (parameters is object && parameters.ContainsKey("request") && parameters["request"] is DishOrderRequest request)
            {
                var response = await _dishesService.Order(key, request);

                if (response.Success)
                {
                    return Ok(response);
                }

                return BadRequest(response.Message);
            }

            return BadRequest();
        }

        [HttpPost]
        [EnableQuery]
        public IHttpActionResult Suggestions(ODataActionParameters parameters)
        {
            if (parameters is object && parameters.ContainsKey("request") && parameters["request"] is SuggestionRequest request)
            {
                var response = _dishesService.GetSuggestions(request);

                if (response.Success)
                {
                    return Ok(response.Restaurants);
                }

                return BadRequest(response.Message);
            }

            return BadRequest();
        }

        [HttpPost]
        public IHttpActionResult AddTags([FromODataUri] int key, ODataActionParameters parameters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var request = parameters["request"] as DishTagsRequest;

            var dish = _set.Find(key);

            var tags = _context.Tags.Where(w => request.Tags.Contains(w.Id)).ToList();

            foreach (var tag in tags)
            {
                dish.Tags.Add(tag);
            }

            _context.SaveChanges();

            return Updated(dish);
        }

        [HttpPost]
        public IHttpActionResult RemoveTags([FromODataUri] int key, ODataActionParameters parameters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var request = parameters["request"] as DishTagsRequest;

            var dish = _set.Find(key);

            var tags = _context.Tags.Where(w => request.Tags.Contains(w.Id)).ToList();

            foreach (var tag in tags)
            {
                dish.Tags.Remove(tag);
            }

            _context.SaveChanges();

            return Updated(dish);
        }

    }
}