﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class FeedbacksController : CRUDControllerBase<Feedback>
    {
        public FeedbacksController(NvDbContext context) : base(context) { }
    }
}