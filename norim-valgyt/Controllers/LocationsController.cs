﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class LocationsController : CRUDControllerBase<Location>
    {
        public LocationsController(NvDbContext context) : base(context) { }
    }
}