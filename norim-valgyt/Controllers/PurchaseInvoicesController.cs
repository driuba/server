﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class PurchaseInvoicesController : CRUDControllerBase<PurchaseInvoice>
    {
        public PurchaseInvoicesController(NvDbContext context) : base(context) { }
    }
}