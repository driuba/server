﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class QuestionsController : CRUDControllerBase<Question>
    {
        public QuestionsController(NvDbContext context) : base(context) { }
    }
}