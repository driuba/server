﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class RestaurantsController : CRUDControllerBase<Restaurant>
    {
        public RestaurantsController(NvDbContext context) : base(context) { }
    }
}