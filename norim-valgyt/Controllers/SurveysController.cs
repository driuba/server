﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class SurveysController : CRUDControllerBase<Survey>
    {
        public SurveysController(NvDbContext context) : base(context) { }
    }
}