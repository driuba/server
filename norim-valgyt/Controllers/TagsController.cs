﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class TagsController : CRUDControllerBase<Tag>
    {
        public TagsController(NvDbContext context) : base(context) { }
    }
}