﻿using NorimValgyt.Context;
using NorimValgyt.Models;

namespace NorimValgyt.Controllers
{
    public class UsersController : CRUDControllerBase<User>
    {
        public UsersController(NvDbContext context) : base(context) { }
    }
}