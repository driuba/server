﻿namespace NorimValgyt.Dto
{
    public class AnswerDto
    {
        public int QuestionId { get; set; }

        public double Score { get; set; }
    }
}