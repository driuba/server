﻿using NorimValgyt.Models;

namespace NorimValgyt.Dto
{
    public class DishOrderRequest
    {
        public string Address { get; set; }

        public bool Locate { get; set; }

        public bool OrderDelivery { get; set; }

        public int UserId { get; set; }
    }
}