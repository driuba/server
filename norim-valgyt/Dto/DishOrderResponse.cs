﻿using NorimValgyt.Models;

namespace NorimValgyt.Dto
{
    public class DishOrderResponse : ServiceResponse
    {
        public int OrderTime { get; set; }
    }
}