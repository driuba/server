﻿using System.Collections.Generic;

namespace NorimValgyt.Dto
{
    public class DishTagsRequest
    {
        public DishTagsRequest()
        {
            Tags = new List<int>();
        }

        public IEnumerable<int> Tags { get; set; }
    }
}