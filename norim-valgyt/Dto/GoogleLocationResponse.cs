﻿namespace NorimValgyt.Dto
{
    public class GoogleLocationResponse
    {
        public double Latitude { get; set; }
        
        public double Longtitude { get; set; }
    }
}