﻿using System.Collections.Generic;

namespace NorimValgyt.Dto
{
    public class SuggestionRequest
    {
        public SuggestionRequest()
        {
            Answers = new List<AnswerDto>();
        }

        public IEnumerable<AnswerDto> Answers { get; set; }

        public bool Filter { get; set; }

        public bool Locate { get; set; }

        public int UserId { get; set; }
    }
}