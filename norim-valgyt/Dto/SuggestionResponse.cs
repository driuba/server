﻿using NorimValgyt.Models;
using System.Collections.Generic;

namespace NorimValgyt.Dto
{
    public class SuggestionResponse : ServiceResponse
    {
        public IEnumerable<Restaurant> Restaurants { get; set; }
    }
}