namespace NorimValgyt.Enums
{
    public enum State
    {
        Canceled,
        Error,
        Waiting,
        InProgress,
        Completed
    }

}
