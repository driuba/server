using NorimValgyt.App_Start;
using System.Web;
using System.Web.Http;

namespace norim_valgyt
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
