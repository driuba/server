﻿using NorimValgyt.Dto;
using System;

namespace NorimValgyt
{
    public class GoogleApi
    {
        public GoogleLocationResponse Locate()
        {
            var random = new Random();

            return new GoogleLocationResponse
            {
                Latitude = random.NextDouble() * 2 * 50 - 50,
                Longtitude = random.NextDouble() * 2 * 20 - 20                
            };
        }
    }
}