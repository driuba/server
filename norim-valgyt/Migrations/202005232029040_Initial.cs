﻿namespace NorimValgyt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisableTime = c.DateTime(precision: 0),
                        Email = c.String(unicode: false),
                        FirstName = c.String(unicode: false),
                        Hash = c.String(unicode: false),
                        LastName = c.String(unicode: false),
                        Phone = c.String(unicode: false),
                        RegisterTime = c.DateTime(nullable: false, precision: 0),
                        UserName = c.String(unicode: false),
                        Discriminator = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionId = c.Int(nullable: false),
                        Score = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.QuestionId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionText = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Surveys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Dishes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Price = c.Double(nullable: false),
                        RestaurantId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Restaurants", t => t.RestaurantId, cascadeDelete: true)
                .Index(t => t.RestaurantId);
            
            CreateTable(
                "dbo.PurchaseInvoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false, precision: 0),
                        Price = c.Double(nullable: false),
                        State = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Feedbacks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Comment = c.String(unicode: false),
                        Date = c.DateTime(nullable: false, precision: 0),
                        RestaurantId = c.Int(nullable: false),
                        Score = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Restaurants", t => t.RestaurantId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RestaurantId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Restaurants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Address = c.String(unicode: false),
                        Latitude = c.Double(),
                        Longitude = c.Double(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DeliveryInvoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CarrierId = c.Int(),
                        LocationId = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        PurchaseInvoiceId = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CarrierId)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.PurchaseInvoices", t => t.PurchaseInvoiceId, cascadeDelete: true)
                .Index(t => t.CarrierId)
                .Index(t => t.LocationId)
                .Index(t => t.PurchaseInvoiceId);
            
            CreateTable(
                "dbo.SurveyQuestions",
                c => new
                    {
                        Survey_Id = c.Int(nullable: false),
                        Question_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Survey_Id, t.Question_Id })
                .ForeignKey("dbo.Surveys", t => t.Survey_Id, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.Question_Id, cascadeDelete: true)
                .Index(t => t.Survey_Id)
                .Index(t => t.Question_Id);
            
            CreateTable(
                "dbo.PurchaseInvoiceDishes",
                c => new
                    {
                        PurchaseInvoice_Id = c.Int(nullable: false),
                        Dish_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PurchaseInvoice_Id, t.Dish_Id })
                .ForeignKey("dbo.PurchaseInvoices", t => t.PurchaseInvoice_Id, cascadeDelete: true)
                .ForeignKey("dbo.Dishes", t => t.Dish_Id, cascadeDelete: true)
                .Index(t => t.PurchaseInvoice_Id)
                .Index(t => t.Dish_Id);
            
            CreateTable(
                "dbo.DishTags",
                c => new
                    {
                        Dish_Id = c.Int(nullable: false),
                        Tag_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Dish_Id, t.Tag_Id })
                .ForeignKey("dbo.Dishes", t => t.Dish_Id, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .Index(t => t.Dish_Id)
                .Index(t => t.Tag_Id);
            
            CreateTable(
                "dbo.TagQuestions",
                c => new
                    {
                        Tag_Id = c.Int(nullable: false),
                        Question_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_Id, t.Question_Id })
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.Question_Id, cascadeDelete: true)
                .Index(t => t.Tag_Id)
                .Index(t => t.Question_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagQuestions", "Question_Id", "dbo.Questions");
            DropForeignKey("dbo.TagQuestions", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.DishTags", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.DishTags", "Dish_Id", "dbo.Dishes");
            DropForeignKey("dbo.PurchaseInvoices", "UserId", "dbo.Users");
            DropForeignKey("dbo.Feedbacks", "UserId", "dbo.Users");
            DropForeignKey("dbo.Restaurants", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.DeliveryInvoices", "PurchaseInvoiceId", "dbo.PurchaseInvoices");
            DropForeignKey("dbo.DeliveryInvoices", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.DeliveryInvoices", "CarrierId", "dbo.Users");
            DropForeignKey("dbo.Feedbacks", "RestaurantId", "dbo.Restaurants");
            DropForeignKey("dbo.Dishes", "RestaurantId", "dbo.Restaurants");
            DropForeignKey("dbo.Answers", "UserId", "dbo.Users");
            DropForeignKey("dbo.PurchaseInvoiceDishes", "Dish_Id", "dbo.Dishes");
            DropForeignKey("dbo.PurchaseInvoiceDishes", "PurchaseInvoice_Id", "dbo.PurchaseInvoices");
            DropForeignKey("dbo.SurveyQuestions", "Question_Id", "dbo.Questions");
            DropForeignKey("dbo.SurveyQuestions", "Survey_Id", "dbo.Surveys");
            DropForeignKey("dbo.Answers", "QuestionId", "dbo.Questions");
            DropIndex("dbo.TagQuestions", new[] { "Question_Id" });
            DropIndex("dbo.TagQuestions", new[] { "Tag_Id" });
            DropIndex("dbo.DishTags", new[] { "Tag_Id" });
            DropIndex("dbo.DishTags", new[] { "Dish_Id" });
            DropIndex("dbo.PurchaseInvoiceDishes", new[] { "Dish_Id" });
            DropIndex("dbo.PurchaseInvoiceDishes", new[] { "PurchaseInvoice_Id" });
            DropIndex("dbo.SurveyQuestions", new[] { "Question_Id" });
            DropIndex("dbo.SurveyQuestions", new[] { "Survey_Id" });
            DropIndex("dbo.DeliveryInvoices", new[] { "PurchaseInvoiceId" });
            DropIndex("dbo.DeliveryInvoices", new[] { "LocationId" });
            DropIndex("dbo.DeliveryInvoices", new[] { "CarrierId" });
            DropIndex("dbo.Restaurants", new[] { "LocationId" });
            DropIndex("dbo.Feedbacks", new[] { "UserId" });
            DropIndex("dbo.Feedbacks", new[] { "RestaurantId" });
            DropIndex("dbo.PurchaseInvoices", new[] { "UserId" });
            DropIndex("dbo.Dishes", new[] { "RestaurantId" });
            DropIndex("dbo.Answers", new[] { "UserId" });
            DropIndex("dbo.Answers", new[] { "QuestionId" });
            DropTable("dbo.TagQuestions");
            DropTable("dbo.DishTags");
            DropTable("dbo.PurchaseInvoiceDishes");
            DropTable("dbo.SurveyQuestions");
            DropTable("dbo.DeliveryInvoices");
            DropTable("dbo.Locations");
            DropTable("dbo.Restaurants");
            DropTable("dbo.Feedbacks");
            DropTable("dbo.PurchaseInvoices");
            DropTable("dbo.Dishes");
            DropTable("dbo.Tags");
            DropTable("dbo.Surveys");
            DropTable("dbo.Questions");
            DropTable("dbo.Answers");
            DropTable("dbo.Users");
        }
    }
}
