﻿namespace NorimValgyt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Necessary_Mods : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Available", c => c.Boolean());
            AddColumn("dbo.Restaurants", "CloseTime", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Restaurants", "OpenTime", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Restaurants", "OpenTime");
            DropColumn("dbo.Restaurants", "CloseTime");
            DropColumn("dbo.Users", "Available");
        }
    }
}
