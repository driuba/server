﻿// <auto-generated />
namespace NorimValgyt.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class surveyName : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(surveyName));
        
        string IMigrationMetadata.Id
        {
            get { return "202005242220180_surveyName"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
