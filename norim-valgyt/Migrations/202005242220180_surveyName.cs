﻿namespace NorimValgyt.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class surveyName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Surveys", "Name", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Surveys", "Name");
        }
    }
}
