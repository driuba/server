namespace NorimValgyt.Models
{
    public class Answer : EntityBase
    {
        public int QuestionId { get; set; }

        public int Score { get; set; }

        public int UserId { get; set; }

        public virtual Question Question { get; set; }

        public virtual User User { get; set; }
    }
}
