using System.Collections.Generic;

namespace NorimValgyt.Models
{
    public class Carrier : User
    {
        public Carrier()
        {
            DeliveryInvoices = new List<DeliveryInvoice>();
        }

        public bool Available { get; set; }

        public virtual ICollection<DeliveryInvoice> DeliveryInvoices { get; set; }
    }
}
