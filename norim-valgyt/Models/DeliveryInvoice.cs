using NorimValgyt.Enums;

namespace NorimValgyt.Models
{
    public class DeliveryInvoice : EntityBase
    {
        public int? CarrierId { get; set; }

        public int LocationId { get; set; }

        public double Price { get; set; }

        public int PurchaseInvoiceId { get; set; }

        public State State { get; set; }

        public virtual Carrier Carrier { get; set; }

        public virtual Location Location { get; set; }

        public virtual PurchaseInvoice PurchaseInvoice { get; set; }
    }
}
