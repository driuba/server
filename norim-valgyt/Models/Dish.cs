using System.Collections.Generic;

namespace NorimValgyt.Models
{
    public class Dish : EntityBase
    {
        public Dish()
        {
            PurchaseInvoices = new List<PurchaseInvoice>();
            Tags = new List<Tag>();
        }

        public string Name { get; set; }

        public double Price { get; set; }

        public int RestaurantId { get; set; }

        public virtual ICollection<PurchaseInvoice> PurchaseInvoices { get; set; }

        public virtual Restaurant Restaurant { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
    }
}
