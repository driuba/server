using System;

namespace NorimValgyt.Models
{
    public class Feedback : EntityBase
    {
        public string Comment { get; set; }

        public DateTime Date { get; set; }

        public int RestaurantId { get; set; }

        public int Score { get; set; }

        public int UserId { get; set; }

        public virtual Restaurant Restaurant { get; set; }

        public virtual User User { get; set; }
    }
}
