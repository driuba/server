using System.Collections.Generic;

namespace NorimValgyt.Models
{
    public class Location : EntityBase
    {
        public Location()
        {
            DeliveryInvoices = new List<DeliveryInvoice>();
        }

        public string Address { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public virtual ICollection<DeliveryInvoice> DeliveryInvoices { get; set; }
    }
}
