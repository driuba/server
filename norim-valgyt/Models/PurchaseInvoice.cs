using NorimValgyt.Enums;
using System;
using System.Collections.Generic;

namespace NorimValgyt.Models
{
    public class PurchaseInvoice : EntityBase
    {
        public PurchaseInvoice()
        {
            Dishes = new List<Dish>();
        }

        public DateTime Date { get; set; }

        public double Price { get; set; }

        public State State { get; set; }

        public int UserId { get; set; }

        public virtual ICollection<Dish> Dishes { get; set; }

        public virtual User User { get; set; }
    }
}
