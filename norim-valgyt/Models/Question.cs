using System.Collections.Generic;

namespace NorimValgyt.Models
{
    public class Question : EntityBase
    {
        public Question()
        {
            Answers = new List<Answer>();
            Surveys = new List<Survey>();
            Tags = new List<Tag>();
        }

        public string QuestionText { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }

        public virtual ICollection<Survey> Surveys { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
    }
}
