using System;
using System.Collections.Generic;

namespace NorimValgyt.Models
{
    public class Restaurant : EntityBase
    {
        public Restaurant()
        {
            Dishes = new List<Dish>();
            Feedbacks = new List<Feedback>();
        }

        public DateTime CloseTime { get; set; }

        public int LocationId { get; set; }

        public string Name { get; set; }

        public DateTime OpenTime { get; set; }

        public virtual ICollection<Dish> Dishes { get; set; }

        public virtual ICollection<Feedback> Feedbacks { get; set; }

        public virtual Location Location { get; set; }
    }
}
