using System.Collections.Generic;

namespace NorimValgyt.Models
{
    public class Survey : EntityBase
    {
        public Survey()
        {
            Questions = new List<Question>();
        }

        public Type Type { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
    }
}
