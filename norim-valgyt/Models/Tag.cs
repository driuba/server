using System.Collections.Generic;

namespace NorimValgyt.Models
{
    public class Tag : EntityBase
    {
        public Tag()
        {
            Dishes = new List<Dish>();
            Questions = new List<Question>();
        }

        public string Name { get; set; }

        public virtual ICollection<Dish> Dishes { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
    }
}
