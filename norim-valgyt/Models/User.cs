using System;
using System.Collections.Generic;

namespace NorimValgyt.Models
{
    public class User : EntityBase
    {
        public User()
        {
            Answers = new List<Answer>();
            Feedbacks = new List<Feedback>();
            PurchaseInvoices = new List<PurchaseInvoice>();
        }

        public DateTime? DisableTime { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string Hash { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public DateTime RegisterTime { get; set; }

        public string UserName { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }

        public virtual ICollection<Feedback> Feedbacks { get; set; }

        public virtual ICollection<PurchaseInvoice> PurchaseInvoices { get; set; }
    }
}
