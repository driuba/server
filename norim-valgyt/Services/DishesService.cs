﻿using NorimValgyt.Context;
using NorimValgyt.Dto;
using NorimValgyt.Enums;
using NorimValgyt.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace NorimValgyt.Services
{
    public class DishesService
    {
        private readonly GoogleApi _googleApi;

        public DishesService(GoogleApi googleApi)
        {
            _googleApi = googleApi;
        }

        public async Task<DishOrderResponse> Order(int key, DishOrderRequest request)
        {
            var currrentTime = DateTime.Now.TimeOfDay;

            Dish dish;

            using (var context = new NvDbContext())
            {
                dish = context.Dishes.Find(key);
            }

            if (dish is null)
            {
                return new DishOrderResponse
                {
                    Message = $"Dish {key} not found"
                };
            }

            Restaurant restaurant;

            using (var context = new NvDbContext())
            {
                restaurant = context.Restaurants.Find(dish.RestaurantId);
            }

            if (restaurant.OpenTime.TimeOfDay.CompareTo(currrentTime) > 0 && restaurant.CloseTime.TimeOfDay.CompareTo(currrentTime) < 0)
            {
                return new DishOrderResponse
                {
                    Message = $"Restaurant {restaurant.Name} currently does not work"
                };
            }

            if (request.OrderDelivery)
            {

                IEnumerable<Carrier> carriers;
                using (var context = new NvDbContext())
                {
                    carriers = context.Carriers
                        .AsNoTracking()
                        .Where(w => w.Available)
                        .ToList();
                }

                if (carriers.Any())
                {
                    using (var context = new NvDbContext())
                    {
                        var delivery = context.DeliveryInvoices.Add(new DeliveryInvoice
                        {
                            Location = new Location
                            {
                                Address = request.Address
                            },
                            Price = dish.Price + 5,
                            PurchaseInvoice = new PurchaseInvoice
                            {
                                Date = DateTime.Now,
                                Dishes = new List<Dish>()
                                {
                                    dish
                                },
                                Price = dish.Price,
                                State = State.Waiting,
                                UserId = request.UserId
                            },
                            State = State.Waiting
                        });

                        var deliveryTime = 0;

                        if (request.Locate)
                        {
                            var location = _googleApi.Locate();

                            delivery.Location.Latitude = location.Latitude;
                            delivery.Location.Longitude = location.Longtitude;

                            deliveryTime += Math.Abs((int)(location.Latitude / location.Longtitude));
                        }

                        deliveryTime = Math.Min(deliveryTime, 15);

                        await context.SaveChangesAsync();

                        return new DishOrderResponse
                        {
                            Success = true,
                            OrderTime = deliveryTime
                        };
                    }
                }

                return new DishOrderResponse
                {
                    Message = "There are no available carriers"
                };
            }

            using (var context = new NvDbContext())
            {
                context.PurchaseInvoices.Add(new PurchaseInvoice
                {
                    Date = DateTime.Now,
                    Dishes = new List<Dish>()
                    {
                        dish
                    },
                    Price = dish.Price + 3,
                    State = State.Waiting,
                    UserId = request.UserId
                });

                await context.SaveChangesAsync();
            }

            return new DishOrderResponse
            {
                Success = true,
                OrderTime = 15
            };
        }

        public SuggestionResponse GetSuggestions(SuggestionRequest request)
        {
            var requestQuestionIds = request.Answers.Select(s => s.QuestionId).Distinct().ToHashSet();

            using (var context = new NvDbContext())
            {
                var questions = context.Questions
                    .AsNoTracking()
                    .Where(w => requestQuestionIds.Contains(w.Id))
                    .ToList();

                var responseQuestionIds = questions.Select(s => s.Id);

                var invalidQuestionIds = requestQuestionIds.Except(responseQuestionIds);

                if (invalidQuestionIds.Any())
                {
                    return new SuggestionResponse
                    {
                        Message = $"The following questions were not found: {string.Join(", ", invalidQuestionIds)}"
                    };
                }
            }

            using (var context = new NvDbContext())
            {
                var user = context.Users.Find(request.UserId);

                if (user is null)
                {
                    return new SuggestionResponse
                    {
                        Message = $"User {request.UserId} not found"
                    };
                }
            }

            using (var context = new NvDbContext())
            {
                var restaurantsQuery = context.Restaurants
                    .AsNoTracking()
                    .AsQueryable()
                    .Include(i => i.Dishes);

                if (request.Locate)
                {
                    var location = _googleApi.Locate();

                    restaurantsQuery = restaurantsQuery
                        .ToList()
                        .OrderBy(o =>
                        {
                            var latitude = Math.Pow(o.Location.Latitude ?? 0 - location.Latitude, 2);
                            var longitude = Math.Pow(o.Location.Longitude ?? 0 - location.Longtitude, 2);

                            return Math.Sqrt(latitude + longitude);
                        })
                        .AsQueryable();
                }

                var requestTags = context.Questions
                        .AsNoTracking()
                        .Where(w => requestQuestionIds.Contains(w.Id))
                        .SelectMany(sm => sm.Answers)
                        .SelectMany(sm => sm.Question.Tags.Select(s => new
                        {
                            Score = sm.Score,
                            TagId = s.Id
                        }));

                if (request.Filter)
                {
                    requestTags = context.Users
                        .AsNoTracking()
                        .SelectMany(sm => sm.Answers)
                        .SelectMany(sm => sm.Question.Tags.Select(s => new
                        {
                            Score = sm.Score,
                            TagId = s.Id
                        }))
                        .Concat(requestTags);
                }

                var userTags = requestTags
                        .GroupBy(g => g.TagId)
                        .Select(s => new
                        {
                            Score = s.Sum(sm => sm.Score),
                            TagId = s.Key
                        })
                        .ToDictionary(k => k.TagId, v => v.Score);

                var restaurantTags = context.Restaurants
                    .AsNoTracking()
                    .Select(s => new
                    {
                        Id = s.Id,
                        Tags = s.Dishes
                            .SelectMany(sm => sm.Tags)
                            .Select(t => t.Id)
                            .Distinct()
                    })
                    .ToList();

                restaurantsQuery = restaurantsQuery
                    .ToList()
                    .Join(
                        restaurantTags,
                        o => o.Id,
                        i => i.Id,
                        (o, i) => new
                        {
                            Restaurant = o,
                            Tags = i.Tags
                        }
                    )
                    .OrderByDescending(o => o.Tags.Sum(s =>
                    {
                        userTags.TryGetValue(s, out var value);

                        return value;
                    })
                    )
                    .Select(s => s.Restaurant)
                    .AsQueryable();

                return new SuggestionResponse
                {
                    Restaurants = restaurantsQuery.ToList(),
                    Success = true
                };
            }
        }
    }
}

